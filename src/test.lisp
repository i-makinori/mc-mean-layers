

;;;; layer

@layer-language_static-type

#|

Any exists <=> layer
layer A ::= State:: ,@(static::(Any exists)), Funcs:: @(implementation::(Any layer))

Verv:: (observe)>-->(layer)>-->((state)+(observe_delta+)+(layer_delta+))


|#


(deflayer buffer ()
  )


;; 



;;; layer 30

(deflayer buffer-window ()
  ;; console :: Console()
  ;; XOrg-I/O :: IO()
  ;; popup-type :: Layout
  ;; key-bindings
  )


;;; layer 10 console

(deflayer console ()
  ;; (cons-type console-means)
  ;; implementation
  )

#| old
  ;; keybinds :: key_star
  ;; :: C-f C-b C-n C-p  C-@ M-x
  ;; ;; Dim1+- Dim2+- mark-region-mode command
  ;; keybinds :: merge-to-key_star-if-writteable
  ;; :: ,@(all of characters) C-x_C-s ... &rest
  ;; ;; [,(input)] (save) ... &body ies
  ;; (C-@ : Layer character-list)
|#

(deflayer console-means ()
  ;; (meaned-char) :: canvas-mean function
  )

;;; layer 00 means

(deflayer meaned-char ()
  ;; mean -> (canvas-point)
  )

(deflayer canvas-point ()
  ;; (exist points) :: points -> Dimension dim -> IO
)


;;;; dimension

@dimension

;;; functional observe

#| ::Any functions |#

(defdimension virtual-self ()
  ;; (virtual-self version-space)
  )

;;; chaos_observe

(defdimension display ())

(defdimension mouse ())

(defdimension keyboard ())

(defdimension network ())

(defdimension brain-wave ())

(defdimension space-wave ())

(defdimension apace-space ())


;;;; fixedpoint
  
@fixedpoint